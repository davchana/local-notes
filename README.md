## Local-Notes (LNT)

[![build status](https://gitlab.com/davchana/local-notes/badges/master/build.svg)](https://gitlab.com/davchana/local-notes/commits/master)

A web-app to store & display small notes, using local browser storage.

I used to use yellow post-it notes or scrap-paper-cut-into-quarters to quickly jot down something, & then write it properly on my custom bullet journal.

But those bits of paper are prone to get lost/misplaced + I need paper & pen if I am on the move, whereas my phone always stays with me. So, this app replaces those bits of paper.

Using:Local Storage, HTML, Javascript, AppCache 

Please try this & report any issues at https://gitlab.com/davchana/local-notes/issues



#### Is it any good?

[Yes, it is!!](https://news.ycombinator.com/item?id=3067434)

#### Build Link

main: _[https://notes.apps.dc1.in](https://notes.apps.dc1.in)_

[![https://notes.apps.dc1.in](https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl=https://notes.apps.dc1.in)](https://notes.apps.dc1.in)