//do the things required on every run

//set the date-picker to today's date automatically
//document.getElementById("date").defaultValue = new Date().toISOString().slice(0,10);
today();
var stamp = "LNT_"; //declare it globally

function today(){
  var now = new Date();
  var day = ("0" + now.getDate()).slice(-2);
  var month = ("0" + (now.getMonth() + 1)).slice(-2);
  var today = now.getFullYear()+"-"+(month)+"-"+(day);
  document.getElementById("date").value = today;
}

//##########################################################################################
//read all local storage keys, & srore in array if matches with stamp, and display
//find all the stored data, add rows in tabel for that.
var len = localStorage.length;
var showcase = document.getElementById("showcase");
var ulMain = document.getElementById("ulMain");
for (
var i=0; i<len; i++ ) {
	var key = thisKey(key, i); //console.log(key); //OK
	if(key){
		var todo = JSON.parse(localStorage.getItem(key)); console.log(todo); //string if one, object of multiple
    console.log(todo.length);
		key = key.replace(stamp, ""); //console.log(key);
		var liDate = document.createElement("li"); liDate.setAttribute("class", "date");//<li class="date">
    liDate.innerHTML = key + " " + getDayName(key, 3);//console.log(liDate);
    var ulTodo = document.createElement("ul"); ulTodo.setAttribute("class", "ulTodo");
		for(var j = 0; j<todo.length; j++){
			var liTodo = document.createElement("li"); liTodo.setAttribute("class", "liTodo");//<li class="todo">
      var htmlString = "<label><input type=checkbox name=del[] value=" + (key+j) + ">" + (todo[j]) + "</label>"; //console.log(htmlString);
			liTodo.innerHTML = htmlString;
      ulTodo.appendChild(liTodo); //console.log(ulTodo);
		}
    liDate.appendChild(ulTodo);
	} //if ends
  ulMain.appendChild(liDate);
} // for loop ends

showcase.appendChild(ulMain);


//##########################################################################################
// return key name from local storage if it is of this project's && is not ts (timestamp)
// will return false if the key is not of this stamp OR is ts (timestamp)
function thisKey (key, i){
	key = localStorage.key(i); //key is already defined in function parameters
	var thiskey = key.search(stamp); //console.log(thiskey); //OK //Process
	var thists = key.search("ts"); //console.log(thists); //OK //No ts keys should be touched
	if(thiskey === 0 && thists !== 4){ //if key is of this project & is not timestamp
		return key;
	} else {
		return false;
	}
}

//##########################################################################################
// create a single list element with todo data
function createlitodo(todo){
	//litodo.appendChild(todo); //<li>ToDo01</li>
	return litodo;
}

//##########################################################################################
//fetch data from form, stringyfy it, add to local storage
function addData(){
var todo = htmlEntity(document.getElementById("todo").value); //console.log(todo);
var key = document.getElementById("date").value; //get date value
if(todo === ""){ //if textarea is empty, do nothing
	return false;
}
var data = JSON.parse(localStorage.getItem(stamp + key));
//console.log(data);// return false;
// === null if no date
if(data === null){ //if date does NOT exist
	data = []; //data is null, make it array
}

data.push(todo);

//console.log(data);

localStorage.setItem(stamp+key, JSON.stringify(data));
localStorage.setItem(stamp+"ts", timestamp());
window.location.reload();
}
//################################################################################
//delete data
function delData (){
var del = document.getElementsByName("del[]");
var delDays = [];
for (var i=0, l=del.length; i < l; i++) {
	if (del[i].checked) {
		//console.log(del[i].value); //OK
		delete window.localStorage[stamp + del[i].value];
	}
}
localStorage.setItem(stamp+"ts", timestamp());
window.location.reload();	
}

//################################################################################
// print page (amend title, send print, amend title back
function printPage(){
	var title = document.title; //console.log(title);
	var tempTitle = title + " - " + timestamp(true);
	document.title = tempTitle;
	window.print();
	document.title = title;
}

//################################################################################
//return string with < & > replaces with entity codes, & space with <br> & nbsp; if ws=true
function htmlEntity(k, ws=false){
	k = k.replace(/(<)+/gmu, "&lt;");// console.log(k); //	/(<)+/g
	k = k.replace(/(>)+/gmu, '&gt;');
	if(ws){
		k = k.replace(/(?:\r\n|\r|\n)/g, '<br>'); //this needs to be before &nbsp;
		k = k.replace(/\s/g, '&nbsp;');
	}
	return k;
}

//################################################################################
// to return timestamp, to save the key for syncing with online store
// if passed true as parameters, returns date as yyyyddmmhhmmssttt, otherwise as UNIX milliseconds stamp (default)
function timestamp(readable=false){
	var ts = new Date();
	//console.log(timestamp.getTime());
	if(readable){
		function pad(s) {
			return (s < 10) ? '0' + s : s;
		}
		return [ts.getFullYear(), pad(ts.getMonth()+1), pad(ts.getDate()), pad(ts.getHours()), pad(ts.getMinutes()), pad(ts.getSeconds()), pad(ts.getMilliseconds())].join("");
	}
	return ts.getTime();
}

//################################################################################
//return day name (1/2/3/full long, default false=full)
function getDayName(date, len=false){
var d = new Date(date);
var weekday = new Array(7);
	weekday[0]=  "Sunday";
	weekday[1] = "Monday";
	weekday[2] = "Tuesday";
	weekday[3] = "Wednesday";
	weekday[4] = "Thursday";
	weekday[5] = "Friday";
	weekday[6] = "Saturday";
var dayname = weekday[d.getDay()];
if(len){
	dayname = dayname.substring(0, len);
}
//console.log(date);
if(date === new Date().toISOString().slice(0,10)){
	dayname = dayname + " &lt;&lt;";
}
return (dayname);
}

//##########################################################################################
// give user a JSON file with all keys from this project's data
function downloadFile() {
	var len = localStorage.length;
	var string = {};
	for ( var i=0; i<len; i++ ) {
		var key = thisKey(key, i); //console.log(key);
		if(key){ string[key] = JSON.parse(localStorage.getItem(key)); }
	}
	string[stamp + "ts"] = localStorage.getItem(stamp + "ts");
	string = JSON.stringify(string, null, 2); //console.log(string); return false;
	var link = document.createElement('a');
	link.download = stamp + timestamp(true) + ".json";
	var blob = new Blob([string], {type: 'text/plain'});
	link.href = window.URL.createObjectURL(blob);
	link.click();
}
//##########################################################################################
// decrease/increase date field to one
function decIncDate(x) 	{
	var temp = new Date(document.getElementById("date").value);
	document.getElementById("date").value = new Date(temp.getTime() + (x*24*60*60*1000)).toISOString().slice(0,10);
	document.getElementById("todo").focus();
	
}

//##########################################################################################
// check if appcache updated?
//minified with dialogue box // https://gitlab.com/snippets/1670884
window.addEventListener("load",function(a){window.applicationCache.addEventListener("updateready",function(a){window.applicationCache.status==window.applicationCache.UPDATEREADY&&confirm("AppCache Updated!!")&&window.location.reload()},!1)},!1);